#!/bin/bash
set +u

#who set the shell to noclobber, no file overwriting allowed!
set +o noclobber

#preset some vars
checkversion=""
domesh=""
arrays=1

i=$#
# any args ?
while [ $i -gt 0 ] ; do
  getopts 'c:s:hfma' x
  case "${x}" in
        a) arrays=0;; # switch test off!
        f) foreground=1;;
        c) checkversion=$OPTARG;;
        m) domesh="yes";;
        s) specd=$OPTARG;;
        h) echo ; echo -e "\
        a) arrays=0;;\n\
        f) foreground=1;;\n\
        c) checkversion=\$OPTARG;;\n\
        s) specd=\$OPTARG;;\n\
        m) domesh=\"yes\";;\n\
"

       exit ;;
  esac
  i=$[i - 1]
done

# some checks

# we need a Display
if [ -z "${DISPLAY}" ]; then
  echo  "no display available, so the test is abandoned"
  exit 1
fi

# with python 2 getting old, it is not obvious to find.
# on debian6 python -> debian2.6, debian8: python -> python2.7.9
# ubuntu : no python, but python2 -> python2.7.18
export python2=python2
if [ -z "$(type -p ${python2})" ]; then python2=python; fi


. blissrc

PYTHONPATH=${PYTHONPATH}${PYTHONPATH:+:}/users/blissadm/python/bliss_modules:/users/blissadm/python/bliss_modules/${BLOS} export PYTHONPATH

if [ -n "${specd}" ]; then
    if [[ ! "$specd" =~ ^/.* ]]; then
        echo "option -s must start with a \"/\"!"
        exit
    fi
    export SPECD=${specd}
fi

macfiles='multim sps'
for file in $macfiles; do
# we need the multim.mac file
  if [ ! -f ${BLISSADM}/spec/macros/${file}.mac ]; then
    echo  "The " ${file} ".mac file is missing! Test abandoned!"
    exit 2
  fi
done

# we need the SpecClient module
if [ ! -d ${BLISSADM}/python/bliss_modules/SpecClient ]; then
  echo  "The SpecClient module is missing! Test abandoned"
  exit 2
fi

SPECNAME=stest$$
NUMBER_POINTS=39

if [ -n "${SPECD}" ]; then
    # create spec version stest - spec test
    # provides config and settings
    spec_version add ${SPECNAME} >&/dev/null
    STESTDIR=${BLISSADM}/local/spec/spec.d/${SPECNAME}
else
    STESTDIR=${SPECD}/${SPECNAME}
    mkdir -p ${STESTDIR}/userfiles
    ln ${SPECD}/../bin/fourc ${SPECD:+$SPECD/../bin/}${SPECNAME}
fi

OLDPWD=`pwd`
cd /tmp
# put out config and settings into place

UNIT=11
CHAN=22
# create the config file
CONFIGFILE=${STESTDIR}/config
cat >${CONFIGFILE} <<EOCONFIG
# ID @(#)getinfo.c      5.38  05/27/11 CSS
# Device nodes
PC_GPIBSIM       =      @gpib_1
SW_SFTWARE       = 1 POLL
# CAMAC Slot Assignments
#  CA_name_unit = slot [crate_number]
# Motor    cntrl steps sign slew base backl accel nada  flags   mne  name
MOT000 =    NONE:0/0   2000  1  2000  200   50  125    0 0x000   unused  unused
MOT001 =    NONE:${UNIT}/${CHAN}   2000  1  2000  200   50  125    0 0x003       m0  Motor 0
MOT002 =    NONE:0/0   2000  1  2000  200   50  125    0 0x003       m1  Motor 1
# Counter   ctrl unit chan scale flags    mne  name
CNT000 =  SFTWARE  0  0   1000 0x001      sec  Seconds
CNT001 =     NONE  0  1      1 0x002      mon  Monitor
CNT002 =     NONE  0  2      1 0x000      det  Detector
CNT003 =     NONE  0  3      1 0x000      el0  el0
EOCONFIG

# create the settings file
TEMPFILE=/tmp/stest-settings$$

/bin/rm -rf ${TEMPFILE}

cat >${TEMPFILE} <<EOSETTINGS
  0          0                0          -100000           100000
  1          0                0          -100000           100000
  2          0                0          -100000           100000
EOSETTINGS

SETTINGSDIR=${STESTDIR}/$(bliss_compat_os ${STESTDIR})
asc2set ${SETTINGSDIR} <${TEMPFILE}
/bin/rm -rf ${TEMPFILE}


# create a macro file with macros we need
cat >1-$$.mac <<EOMACFILE
# Get rid of HKL stuff ...

no_hkl

def multim_read_dummy \'
   if ("motor" in MULTIM[cntr]) {
      xx = (A[MULTIM[cntr]["motnum"]]-MULTIM[cntr]["center"])/MULTIM[cntr]["sigma"]
      value = exp(-xx*xx/2)
      if ("motor2" in MULTIM[cntr]) {
        xx = (A[MULTIM[cntr]["motnum2"]]-MULTIM[cntr]["center"])/MULTIM[cntr]["sigma"]
        value *= exp(-xx*xx/2)
        value *=1000
      }
   } else if ("timeconstant" in jt[cntr]) {
      value = exp(-(time()-MULTIM[cntr]["init_t"])/MULTIM[cntr]["timeconstant"])
   } else
      value = 1

   value += sqrt(value)*(1-rand(10000)/5000)
   value = fabs(value)
   ok = 1
\'

def multim_init_dummy \'
   if ("motor" in MULTIM[cntr]) {
      if ((MULTIM[cntr]["motnum"] = motor_num(MULTIM[cntr]["motor"])) < 0) {
         printf("Invalid motor name: \`%s\`. ", MULTIM[cntr]["motor"])
         return(1)
      } #" for nedit
      if ("motor2" in MULTIM[cntr]) {
         if ((MULTIM[cntr]["motnum2"] = motor_num(MULTIM[cntr]["motor2"])) < 0) {
            printf("Invalid motor name: \`%s\`. ", MULTIM[cntr]["motor2"])
            return(1)
         }
      }
      if (!("fwhm" in MULTIM[cntr])) MULTIM[cntr]["fwhm"] = 1
      MULTIM[cntr]["sigma"] = MULTIM[cntr]["fwhm"]/2.35482
   } else if ("timeconstant" in MULTIM[cntr]) {
      MULTIM[cntr]["timeconstant"] *= 60
      MULTIM[cntr]["init_t"] = time()
   }
\'

def _meshcopytoarray(cnt) \'{
  if (_stype & (scanType_MeshScan | scanType_ScanActive)) { # this is a mesh scan!
    SHOWME[_g1-1][_g2 / _n2-1] = S[cnt]
  }
}
\'

cdef("measure2", "_meshcopytoarray(el0)", "forgetit")


def funcx() \'{
  local retarr[]
  print
  retarr[0] = "arrays are returned correctly"
  retarr[1] = "from macro functions"
  return(retarr)
}
\'

def funcy() \'{
  local answer[]
  local nonarray
  answer    = funcx()
  nonarray  = funcx()
  print answer
}
\'

def funcmp() \'{
    local u, c, mnum
    mnum = 1
    u = motor_par(mnum, "unit")   # should answer ${UNIT}
    c = motor_par(mnum, "channel")  # should answer ${CHAN}

    if ( u != ${UNIT}) {
      tty_cntl("mr")
  eprint "motor_par delivers the wrong unit number"
  tty_cntl("se")
    }
    if ( c != ${CHAN}) {
      tty_cntl("mr")
  eprint "motor_par delivers the wrong channel number"
  tty_cntl("se")
    }
    if ((u == ${UNIT}) && (c == ${CHAN})) {
      eprint "\nmotor_par works fine!"
    }
}
\'

def checkGPIB() \'{
    if ( gpib_cntl("0:21", "responsive") != 0) {
        eprint "Strange, GPIB interface 0 is not defined, but cntl \"responsive\" says 1"
    }
    else
    if ( gpib_cntl("1:21", "responsive") != 1) {
        eprint "GPIB interface does not answer \"responsive\""
    }
    else
        eprint "\nGPIB unit 0 bug not present!"
}
\'
EOMACFILE


# now make the setup file to include the necessary macro files ( to be completed )
cat >> ${STESTDIR}/setup <<EOSETUPADD
jtdo("sps")
shared double array SHOWME[${NUMBER_POINTS}][${NUMBER_POINTS}]
jtdo("multim")
qdo 1-$$.mac
multimsetup el0 type=dummy motor=m0 center=30 fwhm=10 motor2=m1
plotselect el0
EOSETUPADD

    echo setplot 0x5483 >> ${STESTDIR}/setup

# create the python client
cat >4-$$.py <<EOPYFILE
#!/usr/bin/env ${python2}

import sys
from SpecClient import SpecClientError
from SpecClient import SpecWaitObject
from SpecClient import SpecCommand
import SpecClient
SpecClient.setLoggingOff()

EOPYFILE

if [ -n "$domesh" ]; then
cat >>4-$$.py <<EOPYFILE
try:
  mesh = SpecCommand.SpecCommand('mesh', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    mesh("m0", 0, 100, ${NUMBER_POINTS}, "m1", 0, 100, ${NUMBER_POINTS}, 0.001)
  except Exception, err:
    raise
    print "Command failed : %s !" % err

EOPYFILE
fi


cat >>4-$$.py <<EOPYFILE

try:
  eio = SpecCommand.SpecCommand('print "esrf_io  test command: --->", esrf_io("tango://sys/database/2","DevStatus")', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    eio()
  except Exception, err:
    raise
    print "*********************************************"
    print "Command failed : %s !" % err
    print "*********************************************"

try:
  eio = SpecCommand.SpecCommand('print "taco_io  test command: --->", taco_io("tango://sys/database/2","DevStatus")', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    eio()
  except Exception, err:
    raise
    print "*********************************************"
    print "Command failed : %s !" % err
    print "*********************************************"

try:
  eio = SpecCommand.SpecCommand('print "tango_io test command: --->", tango_io("sys/database/2","Status")', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    eio()
  except Exception, err:
    raise
    print "*********************************************"
    print "Command failed : %s !" % err
    print "*********************************************"

EOPYFILE

if [ "${arrays}" == 1 ]; then
cat >>4-$$.py <<EOPYFILE
try:
  arrf = SpecCommand.SpecCommand('funcy()', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    arrf()
  except SpecClient.SpecClientError.SpecClientError:
    print "Problem with ARRAY !!!!!"
    sys.exit()
  except Exception, err:
    raise
    print "Command failed : %s !" % err

try:
  arrf = SpecCommand.SpecCommand('funcmp()', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    arrf()
  except SpecClient.SpecClientError.SpecClientError:
    print "Problem with motor_par !!!!!"
    sys.exit()
  except Exception, err:
    raise
    print "Command failed : %s !" % err

try:
  checkGPIB = SpecCommand.SpecCommand('checkGPIB()', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    checkGPIB()
  except SpecClient.SpecClientError.SpecClientError:
    print "Problem with GPIB position 0 being empty !!!!!"
    sys.exit()
  except Exception, err:
    raise
    print "Command failed : %s !" % err

EOPYFILE
fi

if [ -n "$checkversion" ]; then
cat >>4-$$.py <<EOPYFILE
from SpecClient import SpecVariable

try:
  specver = SpecVariable.SpecVariable('VERSION', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection for VERSION check not possible"
  sys.exit()
else:
  ver = specver.getValue()
  if ver != '${checkversion}':
    print "*******************************************************"
    print "Spec version is not right"
    print " You want ${checkversion}, but you have %s" % ver
    print ' This could mean that the installation of SPEC failed.'
    print "*******************************************************"
    cmd = 'print "Spec version is not right"'
    arrf = SpecCommand.SpecCommand(cmd, 'localhost:${SPECNAME}')
    arrf()
    cmd = 'print "You want ${checkversion}, but you have %s"' % ver
    arrf = SpecCommand.SpecCommand(cmd, 'localhost:${SPECNAME}')
    arrf()
EOPYFILE
fi

# some test for the new (March 2015) hdf5 file format
cat >>4-$$.py <<EOPYFILE
cmd = 'print "Check  hdf5: expect a message about \\\"No HDF5 file open.\\\""'
arrf = SpecCommand.SpecCommand(cmd, 'localhost:${SPECNAME}')
arrf()
try:
  hdfcheck = SpecCommand.SpecCommand('h5_attr()', 'localhost:${SPECNAME}')
except SpecClientError.SpecClientTimeoutError:
  print "Connection not possible"
  sys.exit()
else:
  try:
    hdfcheck()
  except SpecClient.SpecClientError.SpecClientError:
    cmd = 'print "something not right with hdf5"'
    arrf = SpecCommand.SpecCommand(cmd, 'localhost:${SPECNAME}')
    arrf()
  except Exception, err:
    raise
    print "Command failed : %s !" % err
EOPYFILE

cat >>4-$$.py <<EOPYFILE
sys.exit()
EOPYFILE

#export TANGO_HOST=acs:10000
#
#export NETHOST=kidiboo # this caused spec to spit a lot of lines
#          setup_config (): RPC: Procedure unavailable
# this is particular to kidiboo and the NETHOST isn't needed, so take it out.
unset NETHOST

# terminals are funny things. x-terminal-emulator neither works
# on debian8 nor on ub2004. xterm is not installed on ub2004.
TERMINAL=$(type -p xfce4-terminal)
if [ -z "${TERMINAL}" ]; then
    TERMINAL="xterm -geometry -200-200 -sb -sl 5000 "
else
    TERMINAL="${TERMINAL} --geometry=-200-200 "
fi

# start an xterm with our spec inside
${TERMINAL}  -e "${SPECD:+ }${SPECNAME} ${SPECD:+-t esrf -T esrf} -S" >&/dev/null & ALLPIDS=$!

if [ -n "$domesh" ]; then
  case `/csadmin/local/scripts/get_os.share` in
   solaris*|suse*)
    echo "Launching display program: takes long on older systems!"
    PyDis -m -a ${SPECNAME}:SHOWME >&/dev/null& ALLPIDS="$ALLPIDS $!"
    sleep 15
    ;;
   *)
    oxidis -f ${SPECNAME}:SHOWME >&/dev/null& ALLPIDS="$ALLPIDS $!"
    ;;
  esac
fi

# wait some time to let spec settle, then launch the test
sleep 5

# starting python client
${python2} -d 4-$$.py


if [ -z "${foreground}" ]; then
  echo "contemplate the result !! 15 secs "
  sleep 15
  pkill ${SPECNAME}
  kill -9 ${ALLPIDS} >/dev/null 2>&1
fi
wait >/dev/null 2>&1 # for the last background job, our xterm

# finally clean up stuff

/bin/rm -rf ./*$$*
if [ ! "${SPECD}" ]; then
    # and finally destroy our spec version
    echo 'y' | spec_version delete ${SPECNAME} >&/dev/null
else
    echo /bin/rm -rf STESTDIR
fi
